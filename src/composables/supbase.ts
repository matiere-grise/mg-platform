import { inject } from 'vue'
import type { SupabaseClient } from '@supabase/supabase-js'
import { SupabaseType } from '~/modules/supbase'

export function useSupabase(): SupabaseClient {
  const supabase = inject<SupabaseClient>(SupabaseType)
  if (!supabase)
    throw new Error('Supabase provider not found')

  return supabase
}

export function useSupabaseAuth(): SupabaseClient['auth'] {
  const supabase = useSupabase()
  return supabase.auth
}

export function useSupabaseStorage(): SupabaseClient['storage'] {
  const supabase = useSupabase()
  return supabase.storage
}

export function useSupabaseDatabase(): SupabaseClient['from'] {
  const supabase = useSupabase()
  return supabase.from
}
