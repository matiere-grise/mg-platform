import { utils, writeFile } from 'xlsx'

export function convertTable(table: HTMLElement) {
  const sheet = utils.table_to_sheet(table)
  const workbook = utils.book_new()
  utils.book_append_sheet(workbook, sheet, 'Sheet1')
  writeFile(workbook, 'test.xlsx')
}
