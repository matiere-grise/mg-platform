import type { RouteRecordRaw } from 'vue-router'
import { createMemoryHistory, createRouter, createWebHistory } from 'vue-router'
import pages from '~pages'

export const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/shop',
  },
  ...pages,
]

export default createRouter({
  routes,
  history: import.meta.env.SSR ? createMemoryHistory() : createWebHistory(),
})
