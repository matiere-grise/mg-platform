import { acceptHMRUpdate, defineStore } from 'pinia'
import type { MGComponent } from '../component'

interface Worksite {
  id: string
  name: string
  published: boolean
  reference: string
  address: string
  supervisor: string
  contact: string
  publicComment: string
  internalComment: string
  requestEndDate?: string
  recoveryEndDate?: string
  recoveryStartDate?: string
  components?: MGComponent[]
}

export interface RequestTableUserHeader {
  id: string
  city: string
  email: string
  phone: string
  comment: string
  firstname: string
  lastname: string
}

export interface RequestTableComponentHeader {
  id: string
  name: string
  availableQuantity: number
  totalRequestedQuantity: number
}

export interface RequestTableData {
  quantity: number | null
  priority: number | null
}

type CreateWorksite = Omit<Worksite, 'id' | 'components' | 'published'>

export const useWorksiteStore = defineStore('worksite', () => {
  const sup = useSupabase()
  const worksiteDb = () => sup.from('worksites')
  const componentStore = useComponentStore()

  const worksites = ref<Worksite[]>([])
  const currentWorksite = ref<Worksite | null>(null)
  const worksiteSelectedComponentId = ref<string | null>(null)

  async function getAllWorksites() {
    try {
      const { data } = await worksiteDb().select('*')
      worksites.value = data?.map(dbWorksite => ({
        id: dbWorksite.id,
        name: dbWorksite.name,
        reference: dbWorksite.reference,
        requestEndDate: dbWorksite.request_end_date,
        recoveryStartDate: dbWorksite.recovery_start_date,
        recoveryEndDate: dbWorksite.recovery_end_date,
        address: dbWorksite.address,
        supervisor: dbWorksite.supervisor,
        contact: dbWorksite.contact,
        publicComment: dbWorksite.public_comment,
        internalComment: dbWorksite.internal_comment,
        published: dbWorksite.published,
      })) || []
    }
    catch (err) {
      console.error(err)
    }
  }

  async function getWorksiteById(worksiteId: string) {
    try {
      const { data } = await worksiteDb()
        .select('*, components(*)').filter('id', 'eq', worksiteId)

      const dbWorksite = data?.[0]

      currentWorksite.value = {
        id: dbWorksite.id,
        name: dbWorksite.name,
        reference: dbWorksite.reference,
        requestEndDate: dbWorksite.request_end_date,
        recoveryStartDate: dbWorksite.recovery_start_date,
        recoveryEndDate: dbWorksite.recovery_end_date,
        address: dbWorksite.address,
        supervisor: dbWorksite.supervisor,
        contact: dbWorksite.contact,
        publicComment: dbWorksite.public_comment,
        internalComment: dbWorksite.internal_comment,
        published: dbWorksite.published,
        components: dbWorksite.components,
      }
    }
    catch (err) {
      console.error(err)
    }
  }

  async function setComponentVisibility(componentId: string, visibility: boolean) {
    const component = currentWorksite.value?.components?.find((component: MGComponent) => component.id === componentId)

    if (!component)
      return

    await componentStore.updateComponent(componentId, { public: visibility })

    component.public = visibility
  }

  async function createWorksite(worksite: CreateWorksite): Promise<string | null> {
    try {
      const { data, error } = await worksiteDb().insert({
        name: worksite.name,
        reference: worksite.reference,
        request_end_date: worksite.requestEndDate || null,
        recovery_start_date: worksite.recoveryStartDate || null,
        recovery_end_date: worksite.recoveryEndDate || null,
        address: worksite.address,
        supervisor: worksite.supervisor,
        contact: worksite.contact,
        public_comment: worksite.publicComment,
        internal_comment: worksite.internalComment,
      }).select('*')

      const createdWorksite = data?.[0]

      if (error || !createWorksite)
        return null

      worksites.value.push(createdWorksite)

      return createdWorksite.id
    }
    catch (err) {
      console.error(err)
      return null
    }
  }

  async function toggleWorksiteVisibility() {
    if (!currentWorksite.value)
      return

    const worksiteVisibility = !currentWorksite.value.published
    await worksiteDb()
      .update({ published: worksiteVisibility })
      .filter('id', 'eq', currentWorksite.value.id)

    currentWorksite.value.published = worksiteVisibility
  }

  async function generateRepartitionExport() {
    if (!currentWorksite.value)
      return

    const { data: worksiteComponents } = await sup.from('components')
      .select('*')
      .filter('worksite_id', 'eq', currentWorksite.value.id)

    const { data: requests } = await sup.from('wishlists')
      .select('*, wishlist_items(quantity, priority, component_id)')
      // .filter('worksite_id', 'eq', currentWorksite.value.id)

    if (!worksiteComponents || !requests)
      return

    const repartition: RequestTableData[][] = new Array(worksiteComponents.length).fill(0).map(() => new Array(requests.length).fill(null))
    const componentsHeader: RequestTableComponentHeader[] = []
    const userHeader = requests.map<RequestTableUserHeader>(r => ({
      id: r.id,
      firstname: r.firstname,
      lastname: r.lastname,
      city: r.city,
      email: r.email,
      phone: r.phone,
      comment: r.comment,
    }))

    for (const [cIdx, c] of worksiteComponents.entries()) {
      let totalRequestedQuantity = 0
      repartition[cIdx] = []
      for (const [rIdx, r] of requests.entries()) {
        const item = r.wishlist_items?.find((item: any) => item.component_id === c.id)
        repartition[cIdx][rIdx] = {
          quantity: item?.quantity || null,
          priority: item?.priority || null,
        }
        totalRequestedQuantity += item?.quantity || 0
      }
      componentsHeader.push({
        id: c.id,
        name: c.name,
        availableQuantity: c.quantity,
        totalRequestedQuantity,
      })
    }

    return {
      userHeader,
      repartition,
      componentsHeader,
    }
  }

  async function updateWorksite(worksiteId: string, worksite: Partial<Worksite>) {
    try {
      const { data } = await worksiteDb()
        .update({
          name: worksite.name,
          reference: worksite.reference,
          request_end_date: worksite.requestEndDate || null,
          recovery_start_date: worksite.recoveryStartDate || null,
          recovery_end_date: worksite.recoveryEndDate || null,
          address: worksite.address,
          supervisor: worksite.supervisor,
          contact: worksite.contact,
          public_comment: worksite.publicComment,
          internal_comment: worksite.internalComment,
        })
        .filter('id', 'eq', worksiteId)
        .select('*')

      const updatedWorksite = data?.[0]

      if (!updatedWorksite)
        return

      const worksiteIdx = worksites.value.findIndex(w => w.id === worksiteId)
      worksites.value[worksiteIdx] = updatedWorksite
    }
    catch (err) {
      console.error(err)
    }
  }

  async function deleteWorksite(worksiteId: string) {
    if (!worksiteId)
      return

    await worksiteDb().delete().filter('id', 'eq', worksiteId)

    worksites.value = worksites.value.filter(w => w.id !== worksiteId)
  }

  return {
    worksites,
    currentWorksite,
    getWorksiteById,
    getAllWorksites,
    createWorksite,
    updateWorksite,
    deleteWorksite,
    setComponentVisibility,
    toggleWorksiteVisibility,
    generateRepartitionExport,
    worksiteSelectedComponentId,
  }
})

if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useWorksiteStore as any, import.meta.hot))
