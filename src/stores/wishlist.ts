import { acceptHMRUpdate, defineStore } from 'pinia'
import type { MGComponent } from './component'

interface WishlistInfo {
  lastname: string
  firstname: string
  city: string
  email: string
  phone?: string
  usage?: string
}
interface WishlistItem {
  component: MGComponent
  quantity: number
}

export const useWishlistStore = defineStore('wishlist', () => {
  const sup = useSupabase()
  const wishlistTable = () => sup.from('wishlists')
  const wishlistItemTable = () => sup.from('wishlist_items')

  const wishlistInfo = ref<WishlistInfo>({
    lastname: '',
    firstname: '',
    city: '',
    email: '',
    phone: '',
    usage: '',
  })

  const wishlistItem = ref<WishlistItem[]>([])

  const sortedItems = computed({
    get() {
      return wishlistItem.value
    },
    set(items: WishlistItem[]) {
      wishlistItem.value = items
    },
  })

  function addItem(component: MGComponent) {
    wishlistItem.value.push({
      component,
      quantity: 1,
    })
  }

  function removeItem(itemId: string) {
    wishlistItem.value = wishlistItem.value.filter((i: WishlistItem) => i.component.id !== itemId)
  }

  async function saveWishlist() {
    const newList = await wishlistTable().insert({
      firstname: wishlistInfo.value.firstname,
      lastname: wishlistInfo.value.lastname,
      city: wishlistInfo.value.city,
      email: wishlistInfo.value.email,
      phone: wishlistInfo.value.phone,
      project_reason: wishlistInfo.value.usage,
    }).select()

    const listId = newList.data?.[0]?.id

    return wishlistItemTable().insert([
      ...wishlistItem.value.map((item, index) => ({
        priority: index,
        quantity: item.quantity,
        wishlist_id: listId,
        component_id: item.component.id,
      })),
    ])
  }

  async function filterAvailableItems(items: WishlistItem[]): Promise<WishlistItem[]> {
    const { data } = await sup.from('shop_items').select('id')

    if (!data)
      return []

    const availableIds = data.map(i => i.id)

    return items.filter((item: WishlistItem) => availableIds.includes(item.component.id))
  }

  async function getWishByComponentId(componentId: string) {
    const { data } = await wishlistItemTable()
      .select('quantity, priority, wishlist_id (id, firstname, lastname, city, created_at)')
      .filter('component_id', 'eq', componentId)

    return data
  }

  function updateItems(items: WishlistItem[]) {
    wishlistItem.value = items
  }

  function cleanItems() {
    wishlistItem.value = []
  }

  return {
    wishlistInfo,
    wishlistItem,
    addItem,
    updateItems,
    cleanItems,
    removeItem,
    sortedItems,
    saveWishlist,
    getWishByComponentId,
    filterAvailableItems,
  }
})

if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useWishlistStore as any, import.meta.hot))
