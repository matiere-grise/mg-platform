import { acceptHMRUpdate, defineStore } from 'pinia'

export interface MGComponent {
  id: string
  name: string
  public: boolean
  category: string
  quantity: string
  quantityType: string
  photoUrls: string[]
  worksiteId: string
  lot?: string
  size?: string
  mater?: string
  state?: string
  comment?: string
  exposition?: string
  localization?: string
  usageComment?: string
  recoveryMethod?: string
  assemblyMethod?: string
}

export const useComponentStore = defineStore('component', () => {
  const sup = useSupabase()
  const _components = ref<MGComponent[]>([])
  const _worksiteComponents = ref<MGComponent[]>([])

  function setComponents(components: MGComponent[]) {
    _components.value = components
      .filter((component: MGComponent) => !!component.name)
      .map((component: MGComponent) => ({
        ...component,
        photoUrls: component.photoUrls.filter((url: string) => !!url),
      }))
  }

  async function updateComponent(componentId: string, component: Partial<MGComponent>) {
    const { data } = await sup.from('components').update(component).eq('id', componentId).select('*')

    return data
  }

  async function fetchComponents() {
    const { data } = await sup.from('shop_items').select()
    _components.value = data?.map<MGComponent>(dbComponent => ({
      id: dbComponent.id,
      lot: dbComponent.lot,
      name: dbComponent.name,
      size: dbComponent.sizes,
      state: dbComponent.state,
      public: dbComponent.public,
      comment: dbComponent.comment,
      quantity: dbComponent.quantity,
      category: dbComponent.category,
      mater: dbComponent.composition,
      usageComment: dbComponent.usage,
      photoUrls: dbComponent.image_ids,
      exposition: dbComponent.exposition,
      worksiteId: dbComponent.worksite_id,
      localization: dbComponent.localization,
      quantityType: dbComponent.quantity_unit,
      recoveryMethod: dbComponent.recover_method,
      assemblyMethod: dbComponent.assembly_method,
    })) || []
  }

  async function fetchWorksiteComponents(worksiteId: string) {
    const { data } = await sup.from('components').select('*').eq('worksite_id', worksiteId)
    _worksiteComponents.value = data?.map<MGComponent>(dbComponent => ({
      id: dbComponent.id,
      lot: dbComponent.lot,
      name: dbComponent.name,
      size: dbComponent.sizes,
      state: dbComponent.state,
      public: dbComponent.public,
      comment: dbComponent.comment,
      quantity: dbComponent.quantity,
      category: dbComponent.category,
      mater: dbComponent.composition,
      usageComment: dbComponent.usage,
      photoUrls: dbComponent.image_ids,
      exposition: dbComponent.exposition,
      worksiteId: dbComponent.worksite_id,
      localization: dbComponent.localization,
      quantityType: dbComponent.quantity_unit,
      recoveryMethod: dbComponent.recover_method,
      assemblyMethod: dbComponent.assembly_method,
    })) || []
  }

  async function saveComponents(components: MGComponent[]) {
    for (const component of components) {
      await sup.from('components').insert({
        lot: component.lot,
        name: component.name,
        sizes: component.size,
        state: component.state,
        comment: component.comment,
        quantity: component.quantity,
        category: component.category,
        composition: component.mater,
        usage: component.usageComment,
        image_ids: component.photoUrls,
        exposition: component.exposition,
        worksite_id: component.worksiteId,
        localization: component.localization,
        quantity_unit: component.quantityType,
        recover_method: component.recoveryMethod,
        assembly_method: component.assemblyMethod,
      })
    }
  }

  return {
    _components,
    setComponents,
    saveComponents,
    fetchComponents,
    updateComponent,
    _worksiteComponents,
    fetchWorksiteComponents,
  }
})

if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useComponentStore as any, import.meta.hot))
