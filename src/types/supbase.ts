export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      components: {
        Row: {
          assemblyMethod: string | null
          category: string
          comment: string | null
          composition: string | null
          created_at: string | null
          exposition: string | null
          id: string
          imageIds: string[] | null
          localisation: string | null
          lot: string | null
          name: string
          quantity: number
          quantityUnit: string | null
          recoverMethod: string | null
          sizes: string | null
          state: string | null
          usage: string | null
        }
        Insert: {
          assemblyMethod?: string | null
          category: string
          comment?: string | null
          composition?: string | null
          created_at?: string | null
          exposition?: string | null
          id?: string
          imageIds?: string[] | null
          localisation?: string | null
          lot?: string | null
          name: string
          quantity?: number
          quantityUnit?: string | null
          recoverMethod?: string | null
          sizes?: string | null
          state?: string | null
          usage?: string | null
        }
        Update: {
          assemblyMethod?: string | null
          category?: string
          comment?: string | null
          composition?: string | null
          created_at?: string | null
          exposition?: string | null
          id?: string
          imageIds?: string[] | null
          localisation?: string | null
          lot?: string | null
          name?: string
          quantity?: number
          quantityUnit?: string | null
          recoverMethod?: string | null
          sizes?: string | null
          state?: string | null
          usage?: string | null
        }
        Relationships: []
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
