import { createClient } from '@supabase/supabase-js'
import { type UserModule } from '~/types'

export const SupabaseType = Symbol('supabase')

export const install: UserModule = ({ app }) => {
  const url = import.meta.env.VITE_SUPABASE_URL
  const key = import.meta.env.VITE_SUPABASE_KEY

  const supabase = createClient(url, key)
  app.provide(SupabaseType, supabase)
  app.config.globalProperties.$supabase = supabase
}
