import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { VDataTable } from 'vuetify/labs/VDataTable'

import { aliases, mdi } from 'vuetify/iconsets/mdi'

import '@mdi/font/css/materialdesignicons.css'

import { type UserModule } from '~/types'

export const install: UserModule = ({ app, isClient }) => {
  const vuetify = createVuetify({
    components: {
      ...components,
      VDataTable,
    },
    directives,
    ssr: !isClient,
    locale: {
      locale: 'fr',
      fallback: 'en',
    },
    icons: {
      defaultSet: 'mdi',
      aliases,
      sets: {
        mdi,
      },
    },
  })

  app.use(vuetify)
}
